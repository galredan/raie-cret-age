import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { BookingComponent } from './feature/booking/booking.component'
import { CalendarComponent } from './feature/calendar/calendar.component'
import { ContactComponent } from './feature/contact/contact.component'
import { PictureComponent } from './feature/picture/picture.component'
import { PageComponent } from './page/page.component'
import { NotFoundComponent } from './core/not-found/not-found.component'

const routes: Routes = [
    { path: 'accueil', component: PageComponent },
    { path: 'reservation', component: BookingComponent },
    { path: 'calendrier', component: CalendarComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'galerie', component: PictureComponent },

    { path: '', redirectTo: '/accueil', pathMatch: 'full' },
    { path: '**', component: NotFoundComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}

// export const routingComponents = [
//     PageComponent,
//     BookingComponent,
//     CalendarComponent,
//     ContactComponent,
//     PictureComponent
// ]
