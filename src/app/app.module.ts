import { BrowserModule } from '@angular/platform-browser'

import { NgModule } from '@angular/core'
import { MaterialModule } from './material.module'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { LayoutComponent } from './core/layout/layout.component'
import { HeaderComponent } from './core/header/header.component'
import { FooterComponent } from './core/footer/footer.component'
import { NotFoundComponent } from './core/not-found/not-found.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { PictureComponent } from './feature/picture/picture.component'
import { ContactComponent } from './feature/contact/contact.component'
import { CalendarComponent } from './feature/calendar/calendar.component'
import { BookingComponent } from './feature/booking/booking.component'
import { PageComponent } from './page/page.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

@NgModule({
    declarations: [
        AppComponent,
        LayoutComponent,
        HeaderComponent,
        FooterComponent,
        NotFoundComponent,
        NavbarComponent,
        PictureComponent,
        ContactComponent,
        CalendarComponent,
        BookingComponent,
        PageComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule,
        NgxMaterialTimepickerModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
