import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'app-picture',
    templateUrl: './picture.component.html',
    styleUrls: ['./picture.component.scss']
})
export class PictureComponent implements OnInit {
    haircuts: any[] = [
        { imageUrl: './assets/gallery/1.jpg' },
        { imageUrl: './assets/gallery/2.jpg' },
        { imageUrl: './assets/gallery/3.jpg' },
        { imageUrl: './assets/gallery/4.jpg' },
        { imageUrl: './assets/gallery/5.jpg' }
    ]

    constructor() {}

    ngOnInit() {}
}
