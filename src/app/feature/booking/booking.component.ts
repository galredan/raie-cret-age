import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'app-booking',
    templateUrl: './booking.component.html',
    styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
    services: any[] = [
        { value: 'cut', viewValue: 'Coupe' },
        { value: 'massage', viewValue: 'Massage Cuir Chevelu' },
        { value: 'shamp-cut', viewValue: 'Shampooing + Coupe' },
        { value: 'shamp-massage-cut', viewValue: 'Shampooing + Massage + Coupe' },
        { value: 'shamp-cut-brush', viewValue: 'Shampooing + Coupe + Brushing' }
    ]

    // hours: any[] = [
    //     { value: '10:00', viewValue: '10:00' },
    //     { value: '10:00', viewValue: '10:00' },
    //     { value: '10:30', viewValue: '10:30' },
    //     { value: '11:00', viewValue: '11:00' },
    //     { value: '11:30', viewValue: '11:30' },
    //     { value: '12:00', viewValue: '12:00' }
    // ]

    selectedHour

    constructor() {}

    ngOnInit() {}
}
